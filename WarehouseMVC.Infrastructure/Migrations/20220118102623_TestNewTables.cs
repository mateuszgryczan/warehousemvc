﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace WarehouseMVC.Infrastructure.Migrations
{
    public partial class TestNewTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CEOLastName",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CEOName",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Customers",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<byte[]>(
                name: "LogoPic",
                table: "Customers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Regon",
                table: "Customers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CEOLastName",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "CEOName",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "LogoPic",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "Regon",
                table: "Customers");
        }
    }
}
