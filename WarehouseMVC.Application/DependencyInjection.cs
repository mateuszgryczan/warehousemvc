﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using WarehouseMVC.Application.Interfaces;
using WarehouseMVC.Application.Services;

namespace WarehouseMVC.Application
{
    //Dodatkowa na zapisywanie zależności DependencyInjetion
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddTransient<ICustomerService, CustomerService>();
            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            return services;
        }
    }
}
