﻿using AutoMapper;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseMVC.Application.Mapping;

namespace WarehouseMVC.Application.ViewModels.Customer
{
    public class NewCustomerVm : IMapFrom<WarehouseMVC.Domain.Model.Customer>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NIP { get; set; }
        public string REGON { get; set; }
        public bool IsActive { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<NewCustomerVm, WarehouseMVC.Domain.Model.Customer>()
                .ForMember(a => a.IsActive, opt => opt.MapFrom(b => true));
        }
    }

    public class NewCustomerValidation : AbstractValidator<NewCustomerVm>
    {
        public NewCustomerValidation()
        {
            RuleFor(a => a.Id).NotNull();
            RuleFor(a => a.Name).MaximumLength(255);
            RuleFor(a => a.NIP).Length(10);
            RuleFor(a => a.REGON).Length(9, 14);
        }
    }
}
