﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using WarehouseMVC.Application.Mapping;

namespace WarehouseMVC.Application.ViewModels.Customer
{
    public class CustomerForListVm : IMapFrom<WarehouseMVC.Domain.Model.Customer>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NIP { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<WarehouseMVC.Domain.Model.Customer, CustomerForListVm>();
        }
    }
}
