﻿using System;
using System.Collections.Generic;
using System.Text;
using WarehouseMVC.Application.ViewModels.Customer;

namespace WarehouseMVC.Application.Interfaces
{
    public interface ICustomerService
    {
        ListCustomerForListVm GetAllCustomerForList(int pageSize, int pageNumber, string searchString);
        int AddCustomer(NewCustomerVm customer);
        CustomerDetailsVm GetCustomerDetails(int customerId);
    }
}
