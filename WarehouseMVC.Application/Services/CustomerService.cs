﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WarehouseMVC.Application.Interfaces;
using WarehouseMVC.Application.ViewModels.Customer;
using WarehouseMVC.Domain.Interface;
using WarehouseMVC.Domain.Model;

namespace WarehouseMVC.Application.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        //dependency injection przez constructor
        public CustomerService(ICustomerRepository customerRepository, IMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public int AddCustomer(NewCustomerVm customer)
        {
            var cust = _mapper.Map<Customer>(customer);
            var id = _customerRepository.AddCustomer(cust);

            return id;
        }

        public ListCustomerForListVm GetAllCustomerForList(int pageSize, int pageNumber, string searchString)
        {
            var customers = _customerRepository.GetAllActiveCustomers()
                .Where(a => a.Name.StartsWith(searchString))
                .ProjectTo<CustomerForListVm>(_mapper.ConfigurationProvider).ToList();
            var customersToShow = customers.Skip(pageSize * (pageNumber - 1)).Take(pageSize).ToList();
            var customerList = new ListCustomerForListVm()
            {
                Customers = customersToShow,
                CurrentPage = pageNumber,
                PageSize = pageSize,
                SearchString = searchString,
                Count = customers.Count,
            };

            return customerList;
        }

        public CustomerDetailsVm GetCustomerDetails(int customerId)
        {
            var customer = _customerRepository.GetCustomer(customerId);
            var customerVm = _mapper.Map<CustomerDetailsVm>(customer);

            //customerVm.Addresses = new List<AddressForListVm>();
            //customerVm.PhoneNumbers = new List<ContactDetailListVm>();
            //customerVm.Emails = new List<ContactDetailListVm>();

            ////analogiczną czynność dla numery i emaili do zrobienia
            //foreach (var address in customer.Addresses)
            //{
            //    var add = new AddressForListVm()
            //    {
            //        Id = address.Id,
            //        Country = address.Country,
            //        City = address.City
            //    };

            //    customerVm.Addresses.Add(add);
            //}

            return customerVm;
        }
    }
}
