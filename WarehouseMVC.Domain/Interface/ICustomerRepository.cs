﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WarehouseMVC.Domain.Model;

namespace WarehouseMVC.Domain.Interface
{
    public interface ICustomerRepository
    {
        IQueryable<Customer> GetAllActiveCustomers();
        Customer GetCustomer(int customerId);
        int AddCustomer(Customer customer);

    }
}
