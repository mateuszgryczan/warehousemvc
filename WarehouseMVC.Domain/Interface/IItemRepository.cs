﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WarehouseMVC.Domain.Model;
using Type = WarehouseMVC.Domain.Model.Type;

namespace WarehouseMVC.Domain.Interface
{
    public interface IItemRepository
    {
        int AddItem(Item item);
        IQueryable<Item> GetItemsByTypeId(int typeId);
        Item GetItemById(int itemId);
        IQueryable<Tag> GetAllTags();
        IQueryable<Type> GetAllTypes();
    }
}
