﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseMVC.Domain.Model
{
    // relacja many-to-many z Item
    public class Tag
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<ItemTag> itemTags { get; set; }
    }
}
