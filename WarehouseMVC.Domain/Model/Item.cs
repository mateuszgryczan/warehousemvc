﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseMVC.Domain.Model
{
    // relacja many-to-many z Tag
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set;  }
        public int TypeId { get; set; }

        public virtual Type Type { get; set; }

        public ICollection<ItemTag> itemTags { get; set; }
    }
}
