﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseMVC.Domain.Model
{
    // Relacja jeden do wielu - one to many - jeden typ może być przypisany do wielu itemów
    public class Type
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
