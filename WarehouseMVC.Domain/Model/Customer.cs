﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseMVC.Domain.Model
{
    public class Customer
    {
        // relacja one-to-one z CustomerContactInformation
        // relacja one-to-many z ContactDetail
        // relacja one-to-many z Address
        public int Id { get; set; }
        public string Name { get; set; }
        public string NIP { get; set; }
        public string Regon { get; set; }
        public string CEOName { get; set; }
        public string CEOLastName { get; set; }
        public byte[] LogoPic { get; set; }
        //to jest nasza flaga, może być również StatusId lub coś w tym stylu
        //np. 0 - not active, 1 - suspended, 2 - active
        public bool IsActive { get; set; }

        public CustomerContactInformation CustomerContactInformation { get; set; }

        public virtual ICollection<ContactDetail> ContactDetails { get; set; }
        public virtual ICollection<Address> Addresses { get; set; }
    }
}
