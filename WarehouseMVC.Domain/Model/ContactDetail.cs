﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WarehouseMVC.Domain.Model
{
    public class ContactDetail
    {
        public int Id { get; set; }
        public string ContactDetailInformation { get; set; }
        public int ContactDetailId { get; set; }
        public ContactDetailType ContactDetailType { get; set; }
        public int CusomterId { get; set; }
        public Customer Customer { get; set; }
    }
}
