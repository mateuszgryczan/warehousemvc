﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WarehouseMVC.Web.Models;

namespace WarehouseMVC.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        //ViewListOfItems?id=1&name=nazwa
        public IActionResult ViewListOfItems(int id, string name)
        {
            List<Item> items = new List<Item>();
            items.Add(new Item() { Id = 1, Name = "Apple", ItemType = "Grocery" });
            items.Add(new Item() { Id = 2, Name = "Strawberry", ItemType = "Grocery" });
            items.Add(new Item() { Id = 3, Name = "Banana", ItemType = "Grocery" });

            return View(items);
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
