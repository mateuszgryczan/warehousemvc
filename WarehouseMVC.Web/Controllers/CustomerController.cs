﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WarehouseMVC.Application.Interfaces;
using WarehouseMVC.Application.ViewModels.Customer;

namespace WarehouseMVC.Web.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var model = _customerService.GetAllCustomerForList(1, 1, "");

            return View(model);
        }

        //pageSize - jak dużo rekordów na stronie
        //pageNumber - którą stronę wyświetlić
        //searchString - wyszukiwanie po nazwie
        [HttpPost]
        public IActionResult Index(int pageSize, int? pageNumber, string searchString)
        {
            if (!pageNumber.HasValue)
            {
                pageNumber = 1;
            }

            if (searchString is null)
            {
                searchString = String.Empty;
            }

            var model = _customerService.GetAllCustomerForList(pageSize, pageNumber.Value, searchString);

            return View(model);
        }

        [HttpGet]
        public IActionResult ViewCustomer(int id)
        {
            var model = _customerService.GetCustomerDetails(id);

            return View(model);
        }

        [HttpGet]
        public IActionResult AddCustomer()
        {
            return View(new NewCustomerVm());
        }

        [HttpPost]
        public IActionResult AddCustomer(NewCustomerVm model)
        {
            var id = _customerService.AddCustomer(model);
            return RedirectToAction("Index");
        }


        //[HttpPost]
        //public IActionResult AddCustomer(CustomerModel model)
        //{
        //    var id = customerService.AddCustomer(model);
        //    return View();
        //}

        [HttpGet]
        public IActionResult AddNewAddressForClient(int customerId)
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult AddNewAddressForClient(ClientModel model)
        //{
        //    return View();
        //}


    }
}
