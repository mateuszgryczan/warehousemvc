using System;
using System.ComponentModel;

namespace WarehouseMVC.Web.Models
{
    public class Item
    {
        [DisplayName("Identyfikator")]
        public int Id { get; set; }
        [DisplayName("Imie")]
        public string Name { get; set; }
        public string ItemType { get; set; }
    }
}
